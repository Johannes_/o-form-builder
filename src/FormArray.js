import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { FieldArray, arrayPush, arrayRemove, arrayInsert } from 'redux-form';
import { connect } from 'react-redux';
import { Button, ButtonGroup } from 'm-ui-toolbox';
import { passPropsToChildren } from 'h-utils';

// Move to this repository.
import deleteIcon from '../assets/images/icons/trash.svg';
import arrowUpIcon from '../assets/images/icons/arrow-up.svg';
import arrowDownIcon from '../assets/images/icons/arrow-down.svg';

const classes = (className, modifier) =>
  classnames('c-form-builder__form-array', className, {
    [`c-form-builder__${modifier}_form-array`]: modifier
  });

export const InputArray = props => {
  const {
    children,
    name,
    subject,
    formName,
    modifier,
    disabled,
    className,
    formArrayPush,
    isReorderable,
    addClassName
  } = props;
  console.log('Log deleteIcon:', deleteIcon);
  return (
    <div>
      <FieldArray
        name={name}
        className={classes(className, modifier)}
        subject={subject}
        component={({ fields }) => {
          const childrenArray = [];
          const fieldsCount = fields.length;

          for (let i = 0; i < fieldsCount; i++) {
            childrenArray.push(
              <div
                className="l-flex l-flex_align-end"
                key={`dynamic-form-${formName}-array-column-${
                  fields.name
                }-${i}`}
              >
                {passPropsToChildren(children, {
                  formName,
                  namePrefix: `${fields.name}[${i}].`,
                  disabled
                })}
                <ButtonGroup>
                  {isReorderable && (
                    <Button
                      onClick={e => {
                        e.preventDefault();
                        fields.swap(i, i - 1);
                      }}
                      disabled={i == 0}
                      theme="secondary"
                      icon={arrowUpIcon}
                    />
                  )}
                  {isReorderable && (
                    <Button
                      onClick={e => {
                        e.preventDefault();
                        fields.swap(i, i + 1);
                      }}
                      disabled={i + 1 == fields.length}
                      theme="secondary"
                      icon={arrowDownIcon}
                    />
                  )}
                  <Button
                    onClick={e => {
                      e.preventDefault();
                      fields.remove(i);
                    }}
                    theme="danger"
                    icon={deleteIcon}
                  />
                </ButtonGroup>
              </div>
            );
          }

          return childrenArray;
        }}
      />
      <Button
        className={classnames('u-no-margin', addClassName)}
        type="button"
        theme="secondary"
        key={`add-array-item-${name}`}
        modifier="wide"
        onClick={() => {
          formArrayPush(formName, name);
        }}
      >
        +
      </Button>
    </div>
  );
};

InputArray.propTypes = {
  formArrayPush: PropTypes.func.isRequired,
  children: PropTypes.node,
  className: PropTypes.string,
  addClassName: PropTypes.string,
  name: PropTypes.string.isRequired,
  subject: PropTypes.object,
  modifier: PropTypes.string
};

export default connect(null, {
  formArrayPush: arrayPush,
  formArrayRemove: arrayRemove,
  formArrayInsert: arrayInsert
})(InputArray);
