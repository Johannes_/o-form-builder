import React from 'react';
import PropTypes from 'prop-types';
import { Field as ReduxField } from 'redux-form';
import classnames from 'classnames';

import { Input } from 'm-inputs';

export const Field = ({
  children,
  className,
  inputProps,
  name,
  namePrefix = '',
  label,
  placeholder,
  component = Input,
  size = 2,
  width = 1,
  modifier,
  wrapperModifier,
  noEdit = false,
  normalizer,
  disabled,
  validate,
  type,
  min,
  max,
  defaultValue
}) => (
  <div
    className={classnames('c-form-builder__field', className, {
      [`c-form-builder_${wrapperModifier}`]: wrapperModifier,
      'c-form-builder_no-edit': noEdit
    })}
    size={size}
    width={width}
    modifier={wrapperModifier}
  >
    <ReduxField
      disabled={disabled}
      placeholder={placeholder}
      name={`${namePrefix}${name}`}
      validate={validate}
      normalize={normalizer}
      label={label}
      type={type}
      modifier={modifier}
      component={component}
      min={min}
      max={max}
      defaultValue={defaultValue}
      {...inputProps}
    >
      {children}
    </ReduxField>
  </div>
);

Field.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  name: PropTypes.string.isRequired,
  namePrefix: PropTypes.string,
  placeholder: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  label: PropTypes.oneOfType([PropTypes.object, PropTypes]),
  inputProps: PropTypes.object,
  component: PropTypes.func,
  validate: PropTypes.oneOfType([PropTypes.array, PropTypes.func]),
  size: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  modifier: PropTypes.string,
  wrapperModifier: PropTypes.string,
  type: PropTypes.string,
  noEdit: PropTypes.bool,
  min: PropTypes.number,
  max: PropTypes.number
};

export default Field;
