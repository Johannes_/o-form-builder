import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import { FormattedMessage, defineMessages } from 'react-intl';
import classnames from 'classnames';

// Components
import { passPropsToChildren } from 'h-utils';
import { Button, ButtonGroup, FlexWrapper, FlexCell } from 'm-ui-toolbox';

const classes = (className, modifier) =>
  classnames('c-form-builder', className, {
    [`c-form-builder_${modifier}`]: modifier
  });

const buttonClasses = (className, modifier) =>
  classnames('c-form-builder__actions', className, {
    [`c-form-builder_${modifier}_actions`]: modifier
  });

const messages = defineMessages({
  submitButtonText: {
    id: 'form-builder-submit-button-text',
    defaultMessage: 'Submit'
  },
  cancelButtonText: {
    id: 'form-builder-cancel-button-text',
    defaultMessage: 'Cancel'
  }
});

const FormBuilder = ({
  isResetable,
  children,
  className,
  dirty,
  form,
  onSubmit,
  onCancel,
  handleSubmit,
  invalid,
  isLoading,
  modifier,
  actionModifier,
  actionClassName,
  reset,
  footerComponent,
  showFooter
}) => (
  <form
    onSubmit={handleSubmit(onSubmit)}
    className={classes(className, modifier)}
  >
    {children}
    {footerComponent && showFooter
      ? passPropsToChildren(footerComponent, {
          isLoading,
          invalid,
          dirty,
          handleReset: reset
        })
      : showFooter && (
          <FlexWrapper
            className={buttonClasses(actionClassName, actionModifier)}
          >
            {isResetable && (
							<FlexCell>
								<Button
									theme="transparent"
									type="button"
									modifier="wide"
									onClick={() => {
										if (onCancel) {
											onCancel();
										}
										reset();
									}}
								>
									<FormattedMessage {...messages.cancelButtonText} />
								</Button>
							</FlexCell>
						)}
						<FlexCell>
							<Button
								type="submit"
								modifier="wide"
								disabled={invalid || isLoading || !dirty}
								submitting={isLoading}
							>
								<FormattedMessage {...messages.submitButtonText} />
							</Button>
						</FlexCell>
          </FlexWrapper>
        )}
  </form>
);

FormBuilder.defaultProps = {
  actionClassName: '',
  isLoading: false,
  className: '',
  modifier: undefined,
  actionModifier: '',
  footerComponent: undefined,
  isResetable: false,
  showFooter: true
};

FormBuilder.propTypes = {
  handleSubmit: PropTypes.func.isRequired, // Redux-form
  reset: PropTypes.func.isRequired, // Redux-form
  onSubmit: PropTypes.func.isRequired, // actual submit function
  onCancel: PropTypes.func, // follow up reset function
  id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  isLoading: PropTypes.bool,
  isResetable: PropTypes.bool,
  className: PropTypes.string,
  modifier: PropTypes.oneOf(['table-cell', 'one-liner']),
  actionModifier: PropTypes.string,
  actionClassName: PropTypes.string,
  footerComponent: PropTypes.oneOfType([PropTypes.node]),
  showFooter: PropTypes.bool
};

export default reduxForm({
  enableReinitialize: true
})(FormBuilder);
