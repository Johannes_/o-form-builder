'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactIntl = require('react-intl');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _autobind = require('utils/autobind');

var _autobind2 = _interopRequireDefault(_autobind);

var _Tag = require('components/UI/Tag');

var _Tag2 = _interopRequireDefault(_Tag);

var _error = require('images/icon/small/error.svg');

var _error2 = _interopRequireDefault(_error);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// Utils


// Components


// Icons


var classes = function classes(active, hasErrors) {
  var className = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'c-form-builder__menu-item';
  return (0, _classnames2.default)(className, {
    'js-active': active,
    'js-error': hasErrors
  });
};

var FormMenuItem = function (_Component) {
  _inherits(FormMenuItem, _Component);

  function FormMenuItem(props) {
    _classCallCheck(this, FormMenuItem);

    var _this = _possibleConstructorReturn(this, (FormMenuItem.__proto__ || Object.getPrototypeOf(FormMenuItem)).call(this, props));

    (0, _autobind2.default)(_this);
    return _this;
  }

  _createClass(FormMenuItem, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          menuTitle = _props.menuTitle,
          hasErrors = _props.hasErrors,
          handlePhaseChange = _props.handlePhaseChange,
          phaseId = _props.phaseId,
          active = _props.active;


      return _react2.default.createElement(
        'li',
        {
          className: classes(active, hasErrors),
          onClick: function onClick() {
            return handlePhaseChange(phaseId);
          }
        },
        menuTitle.id ? _react2.default.createElement(_reactIntl.FormattedMessage, menuTitle) : { menuTitle: menuTitle },
        hasErrors && _react2.default.createElement(_Tag2.default, {
          theme: 'danger',
          icon: _error2.default,
          modifier: 'menu-badge u-pull-right'
        })
      );
    }
  }]);

  return FormMenuItem;
}(_react.Component);

FormMenuItem.PropTypes = {
  menuTitle: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object]).isRequired,
  hasErrors: _propTypes2.default.bool.isRequired,
  handlePhaseChange: _propTypes2.default.func.isRequired,
  phaseId: _propTypes2.default.number.isRequired,
  active: _propTypes2.default.bool
};

exports.default = FormMenuItem;