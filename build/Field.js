'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Field = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reduxForm = require('redux-form');

var _classnames2 = require('classnames');

var _classnames3 = _interopRequireDefault(_classnames2);

var _mInputs = require('m-inputs');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Field = exports.Field = function Field(_ref) {
  var _classnames;

  var children = _ref.children,
      className = _ref.className,
      inputProps = _ref.inputProps,
      name = _ref.name,
      _ref$namePrefix = _ref.namePrefix,
      namePrefix = _ref$namePrefix === undefined ? '' : _ref$namePrefix,
      label = _ref.label,
      placeholder = _ref.placeholder,
      _ref$component = _ref.component,
      component = _ref$component === undefined ? _mInputs.Input : _ref$component,
      _ref$size = _ref.size,
      size = _ref$size === undefined ? 2 : _ref$size,
      _ref$width = _ref.width,
      width = _ref$width === undefined ? 1 : _ref$width,
      modifier = _ref.modifier,
      wrapperModifier = _ref.wrapperModifier,
      _ref$noEdit = _ref.noEdit,
      noEdit = _ref$noEdit === undefined ? false : _ref$noEdit,
      normalizer = _ref.normalizer,
      disabled = _ref.disabled,
      validate = _ref.validate,
      type = _ref.type,
      min = _ref.min,
      max = _ref.max,
      defaultValue = _ref.defaultValue;
  return _react2.default.createElement(
    'div',
    {
      className: (0, _classnames3.default)('c-form-builder__field', className, (_classnames = {}, _defineProperty(_classnames, 'c-form-builder_' + wrapperModifier, wrapperModifier), _defineProperty(_classnames, 'c-form-builder_no-edit', noEdit), _classnames)),
      size: size,
      width: width,
      modifier: wrapperModifier
    },
    _react2.default.createElement(
      _reduxForm.Field,
      _extends({
        disabled: disabled,
        placeholder: placeholder,
        name: '' + namePrefix + name,
        validate: validate,
        normalize: normalizer,
        label: label,
        type: type,
        modifier: modifier,
        component: component,
        min: min,
        max: max,
        defaultValue: defaultValue
      }, inputProps),
      children
    )
  );
};

Field.propTypes = {
  children: _propTypes2.default.node,
  className: _propTypes2.default.string,
  name: _propTypes2.default.string.isRequired,
  namePrefix: _propTypes2.default.string,
  placeholder: _propTypes2.default.oneOfType([_propTypes2.default.object, _propTypes2.default.string]),
  label: _propTypes2.default.oneOfType([_propTypes2.default.object, _propTypes2.default]),
  inputProps: _propTypes2.default.object,
  component: _propTypes2.default.func,
  validate: _propTypes2.default.oneOfType([_propTypes2.default.array, _propTypes2.default.func]),
  size: _propTypes2.default.oneOfType([_propTypes2.default.number, _propTypes2.default.string]),
  width: _propTypes2.default.oneOfType([_propTypes2.default.number, _propTypes2.default.string]),
  modifier: _propTypes2.default.string,
  wrapperModifier: _propTypes2.default.string,
  type: _propTypes2.default.string,
  noEdit: _propTypes2.default.bool,
  min: _propTypes2.default.number,
  max: _propTypes2.default.number
};

exports.default = Field;