'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reduxForm = require('redux-form');

var _reactRedux = require('react-redux');

var _lodash = require('lodash');

var _classnames2 = require('classnames');

var _classnames3 = _interopRequireDefault(_classnames2);

var _reactIntl = require('react-intl');

var _autobind = require('utils/autobind');

var _autobind2 = _interopRequireDefault(_autobind);

var _formHelpers = require('utils/formHelpers');

var _FormMenuItem = require('components/FormBuilder/FormMenuItem');

var _FormMenuItem2 = _interopRequireDefault(_FormMenuItem);

var _FormPhase = require('components/FormBuilder/FormPhase');

var _FormPhase2 = _interopRequireDefault(_FormPhase);

var _Button = require('components/UI/Button');

var _Button2 = _interopRequireDefault(_Button);

var _ButtonGroup = require('components/UI/ButtonGroup');

var _ButtonGroup2 = _interopRequireDefault(_ButtonGroup);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Utils


// Components


// Icons
var classes = function classes(className, modifier, isSinglePhase) {
  var _classnames;

  return (0, _classnames3.default)('c-form-builder', className, (_classnames = {}, _defineProperty(_classnames, 'c-form-builder_' + modifier, modifier), _defineProperty(_classnames, 'c-form-builder_single-phase', isSinglePhase), _classnames));
};

var FormBuilder = function (_Component) {
  _inherits(FormBuilder, _Component);

  function FormBuilder(props) {
    _classCallCheck(this, FormBuilder);

    var _this = _possibleConstructorReturn(this, (FormBuilder.__proto__ || Object.getPrototypeOf(FormBuilder)).call(this, props));

    _this.state = { activePhaseId: 0 };
    (0, _autobind2.default)(_this);
    return _this;
  }

  _createClass(FormBuilder, [{
    key: 'handlePhaseChange',
    value: function handlePhaseChange(phaseId) {
      this.setState({ activePhaseId: phaseId });
    }
  }, {
    key: 'handleFormSubmit',
    value: function handleFormSubmit(values) {
      var _props = this.props,
          id = _props.id,
          handleUpdate = _props.handleUpdate,
          handleCreate = _props.handleCreate,
          registeredFields = _props.registeredFields,
          initialValues = _props.initialValues;


      var filteredValues = (0, _formHelpers.filterVariablesByFields)(values, registeredFields);
      if (!id && handleCreate) {
        handleCreate(filteredValues, initialValues);
      } else if (id && handleUpdate) {
        handleUpdate(id, filteredValues, initialValues);
      }
    }
  }, {
    key: 'handleClose',
    value: function handleClose() {
      var _props2 = this.props,
          dirty = _props2.dirty,
          handleClose = _props2.handleClose;


      handleClose(dirty);
    }

    // Validate sub-form (for 'in menu' displaying if sub-form is valid)

  }, {
    key: 'isSubFormValid',
    value: function isSubFormValid(syncErrors, subFormFields) {
      // If no syncErrors just return true
      if (!syncErrors) {
        return true;
      }

      // Search if any field in sub-form configuration has a same name as this in syncErrors tree
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = subFormFields[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var fieldRows = _step.value;

          if (fieldRows.hasOwnProperty('inputs')) {
            var _iteratorNormalCompletion2 = true;
            var _didIteratorError2 = false;
            var _iteratorError2 = undefined;

            try {
              for (var _iterator2 = fieldRows.inputs[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                var field = _step2.value;

                if (field.hasOwnProperty('input') && field.input.hasOwnProperty('name')) {
                  if ((0, _lodash.hasIn)(syncErrors, field.input.name)) {
                    return false;
                  }
                }
              }
            } catch (err) {
              _didIteratorError2 = true;
              _iteratorError2 = err;
            } finally {
              try {
                if (!_iteratorNormalCompletion2 && _iterator2.return) {
                  _iterator2.return();
                }
              } finally {
                if (_didIteratorError2) {
                  throw _iteratorError2;
                }
              }
            }
          }
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      return true;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props3 = this.props,
          setup = _props3.setup,
          handleSubmit = _props3.handleSubmit,
          forms = _props3.forms,
          submitFailed = _props3.submitFailed,
          form = _props3.form,
          invalid = _props3.invalid,
          values = _props3.values,
          className = _props3.className,
          modifier = _props3.modifier,
          footerClassName = _props3.footerClassName,
          phasesClassName = _props3.phasesClassName,
          bodyClassName = _props3.bodyClassName,
          footerCancelTitle = _props3.footerCancelTitle,
          footerSubmitTitle = _props3.footerSubmitTitle,
          header = _props3.header,
          _props3$cancelButton = _props3.cancelButton,
          cancelButton = _props3$cancelButton === undefined ? true : _props3$cancelButton,
          footer = _props3.footer,
          isLoading = _props3.isLoading;


      return _react2.default.createElement(
        'form',
        {
          onSubmit: handleSubmit(this.handleFormSubmit),
          className: classes(className, modifier, setup.length === 1)
        },
        header && header(this.handleClose),
        _react2.default.createElement(
          'span',
          { className: (0, _classnames3.default)(bodyClassName, 'c-form-builder__body') },
          _react2.default.createElement(
            'div',
            { className: 'c-form-builder' },
            setup.length > 1 && _react2.default.createElement(
              'ul',
              { className: 'c-form-builder__menu' },
              setup.map(function (menu, key) {
                if (!menu.isVisible || menu.isVisible(values)) {
                  // If there are form phases a menu can be turned on.
                  return _react2.default.createElement(_FormMenuItem2.default, {
                    key: 'menu-item-' + key,
                    phaseId: key,
                    active: _this2.state.activePhaseId === key,
                    hasErrors: submitFailed && form && forms[form] && forms[form].syncErrors && menu.formSetup && !_this2.isSubFormValid(forms[form].syncErrors, menu.formSetup.fieldRows),
                    handlePhaseChange: _this2.handlePhaseChange,
                    menuTitle: menu.menuTitle
                  });
                }
              })
            ),
            _react2.default.createElement(
              'div',
              {
                className: (0, _classnames3.default)('c-form-builder__content-wrapper', phasesClassName)
              },
              setup.map(function (phase, key) {
                if (!phase.isVisible || phase.isVisible(values)) {
                  // One form can contain several phases.
                  return _react2.default.createElement(_FormPhase2.default, {
                    key: 'phased-form-' + key,
                    form: form,
                    formRows: phase.formSetup.fieldRows,
                    isActive: key === _this2.state.activePhaseId,
                    formTitle: phase.formTitle,
                    modifier: phase.formSetup.modifier,
                    values: values
                  });
                }
              })
            )
          )
        ),
        footer || _react2.default.createElement(
          'span',
          { className: (0, _classnames3.default)(footerClassName) },
          _react2.default.createElement(
            _ButtonGroup2.default,
            {
              modifier: modifier === 'inline' ? 'filter-submit' : '',
              className: 'u-pull-right'
            },
            cancelButton && _react2.default.createElement(
              _Button2.default,
              {
                theme: 'secondary',
                type: 'button',
                disabled: isLoading,
                onClick: this.handleClose
              },
              footerCancelTitle && footerCancelTitle.id ? _react2.default.createElement(_reactIntl.FormattedMessage, footerCancelTitle) : footerCancelTitle || 'Cancel'
            ),
            _react2.default.createElement(
              _Button2.default,
              {
                type: 'submit',
                disabled: invalid || isLoading,
                submitting: isLoading
              },
              footerCancelTitle && footerSubmitTitle.id ? _react2.default.createElement(_reactIntl.FormattedMessage, footerSubmitTitle) : footerSubmitTitle || 'Submit'
            )
          )
        )
      );
    }
  }]);

  return FormBuilder;
}(_react.Component);

FormBuilder.defaultProps = {
  isLoading: false
};

FormBuilder.propTypes = {
  handleSubmit: _propTypes2.default.func.isRequired, // passed by reduxForm
  handleUpdate: _propTypes2.default.func,
  handleCreate: _propTypes2.default.func,
  isLoading: _propTypes2.default.bool,
  cancelButton: _propTypes2.default.bool,
  footer: _propTypes2.default.node,
  setup: _propTypes2.default.array.isRequired,
  submitFailed: _propTypes2.default.bool.isRequired,
  id: _propTypes2.default.oneOfType([_propTypes2.default.number, _propTypes2.default.string]),
  initialValues: _propTypes2.default.object,
  registeredFields: _propTypes2.default.object,
  footerClassName: _propTypes2.default.string,
  phasesClassName: _propTypes2.default.string,
  bodyClassName: _propTypes2.default.string,
  values: _propTypes2.default.object,
  modifier: _propTypes2.default.oneOf(['inline'])
};

exports.default = (0, _reduxForm.reduxForm)({})((0, _reactRedux.connect)(function (state, props) {
  return {
    values: state.form[props.form] && state.form[props.form].values,
    registeredFields: state.form[props.form] && state.form[props.form].registeredFields,
    forms: state.form
  };
})(FormBuilder));