'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reduxForm = require('redux-form');

var _reactIntl = require('react-intl');

var _classnames3 = require('classnames');

var _classnames4 = _interopRequireDefault(_classnames3);

var _hUtils = require('h-utils');

var _mUiToolbox = require('m-ui-toolbox');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Components


var classes = function classes(className, modifier) {
  return (0, _classnames4.default)('c-form-builder', className, _defineProperty({}, 'c-form-builder_' + modifier, modifier));
};

var buttonClasses = function buttonClasses(className, modifier) {
  return (0, _classnames4.default)('c-form-builder__actions', className, _defineProperty({}, 'c-form-builder_' + modifier + '_actions', modifier));
};

var messages = (0, _reactIntl.defineMessages)({
  submitButtonText: {
    id: 'form-builder-submit-button-text',
    defaultMessage: 'Submit'
  },
  cancelButtonText: {
    id: 'form-builder-cancel-button-text',
    defaultMessage: 'Cancel'
  }
});

var FormBuilder = function FormBuilder(_ref) {
  var isResetable = _ref.isResetable,
      children = _ref.children,
      className = _ref.className,
      dirty = _ref.dirty,
      form = _ref.form,
      onSubmit = _ref.onSubmit,
      onCancel = _ref.onCancel,
      handleSubmit = _ref.handleSubmit,
      invalid = _ref.invalid,
      isLoading = _ref.isLoading,
      modifier = _ref.modifier,
      actionModifier = _ref.actionModifier,
      actionClassName = _ref.actionClassName,
      reset = _ref.reset,
      footerComponent = _ref.footerComponent,
      showFooter = _ref.showFooter;
  return _react2.default.createElement(
    'form',
    {
      onSubmit: handleSubmit(onSubmit),
      className: classes(className, modifier)
    },
    children,
    footerComponent && showFooter ? (0, _hUtils.passPropsToChildren)(footerComponent, {
      isLoading: isLoading,
      invalid: invalid,
      dirty: dirty,
      handleReset: reset
    }) : showFooter && _react2.default.createElement(
      _mUiToolbox.FlexWrapper,
      {
        className: buttonClasses(actionClassName, actionModifier)
      },
      isResetable && _react2.default.createElement(
        _mUiToolbox.FlexCell,
        null,
        _react2.default.createElement(
          _mUiToolbox.Button,
          {
            theme: 'transparent',
            type: 'button',
            modifier: 'wide',
            onClick: function onClick() {
              if (onCancel) {
                onCancel();
              }
              reset();
            }
          },
          _react2.default.createElement(_reactIntl.FormattedMessage, messages.cancelButtonText)
        )
      ),
      _react2.default.createElement(
        _mUiToolbox.FlexCell,
        null,
        _react2.default.createElement(
          _mUiToolbox.Button,
          {
            type: 'submit',
            modifier: 'wide',
            disabled: invalid || isLoading || !dirty,
            submitting: isLoading
          },
          _react2.default.createElement(_reactIntl.FormattedMessage, messages.submitButtonText)
        )
      )
    )
  );
};

FormBuilder.defaultProps = {
  actionClassName: '',
  isLoading: false,
  className: '',
  modifier: undefined,
  actionModifier: '',
  footerComponent: undefined,
  isResetable: false,
  showFooter: true
};

FormBuilder.propTypes = {
  handleSubmit: _propTypes2.default.func.isRequired, // Redux-form
  reset: _propTypes2.default.func.isRequired, // Redux-form
  onSubmit: _propTypes2.default.func.isRequired, // actual submit function
  onCancel: _propTypes2.default.func, // follow up reset function
  id: _propTypes2.default.oneOfType([_propTypes2.default.number, _propTypes2.default.string]),
  isLoading: _propTypes2.default.bool,
  isResetable: _propTypes2.default.bool,
  className: _propTypes2.default.string,
  modifier: _propTypes2.default.oneOf(['table-cell', 'one-liner']),
  actionModifier: _propTypes2.default.string,
  actionClassName: _propTypes2.default.string,
  footerComponent: _propTypes2.default.oneOfType([_propTypes2.default.node]),
  showFooter: _propTypes2.default.bool
};

exports.default = (0, _reduxForm.reduxForm)({
  enableReinitialize: true
})(FormBuilder);