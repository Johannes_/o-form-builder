'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InputArray = undefined;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames2 = require('classnames');

var _classnames3 = _interopRequireDefault(_classnames2);

var _reduxForm = require('redux-form');

var _reactRedux = require('react-redux');

var _mUiToolbox = require('m-ui-toolbox');

var _hUtils = require('h-utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Move to this repository.
var deleteIcon = function deleteIcon(props) {
  return _react2.default.createElement(
    'svg',
    props,
    _react2.default.createElement(
      'title',
      null,
      'icon/small/trash'
    ),
    _react2.default.createElement(
      'g',
      {
        fill: 'none',
        fillRule: 'evenodd'
      },
      _react2.default.createElement('path', {
        d: 'M8.5 2.5v-1a1 1 0 0 0-1.01-1H5.51c-.558 0-1.01.444-1.01 1v1h4zM12.5 2.5H.5v1l1 .5.434 6.504c.036.55.512.996 1.064.996h7.004c.551 0 1.028-.447 1.064-.996L11.5 4l1-.5v-1zM4.5 10V4M8.5 10V4M6.5 10V4'
      })
    )
  );
};

deleteIcon.defaultProps = {
  width: '13',
  height: '13',
  viewBox: '0 0 13 13',
  xmlns: 'http://www.w3.org/2000/svg'
};

var arrowUpIcon = function arrowUpIcon(props) {
  return _react2.default.createElement(
    'svg',
    props,
    _react2.default.createElement(
      'title',
      null,
      'icon/small/arrow-up'
    ),
    _react2.default.createElement(
      'g',
      {
        stroke: '#000',
        fill: 'none',
        fillRule: 'evenodd'
      },
      _react2.default.createElement('path', {
        d: 'M6 5.5H3l3.5-3 3.5 3H7M5.5 5.5v5h2v-5'
      })
    )
  );
};

arrowUpIcon.defaultProps = {
  width: '13',
  height: '13',
  viewBox: '0 0 13 13',
  xmlns: 'http://www.w3.org/2000/svg'
};

var arrowDownIcon = function arrowDownIcon(props) {
  return _react2.default.createElement(
    'svg',
    props,
    _react2.default.createElement(
      'title',
      null,
      'icon/small/arrow-explicit-down'
    ),
    _react2.default.createElement(
      'g',
      {
        stroke: '#000',
        fill: 'none',
        fillRule: 'evenodd'
      },
      _react2.default.createElement('path', {
        d: 'M6 7.5H3l3.5 3 3.5-3H7M5.5 7.5v-5h2v5'
      })
    )
  );
};

arrowDownIcon.defaultProps = {
  width: '13',
  height: '13',
  viewBox: '0 0 13 13',
  xmlns: 'http://www.w3.org/2000/svg'
};


var classes = function classes(className, modifier) {
  return (0, _classnames3.default)('c-form-builder__form-array', className, _defineProperty({}, 'c-form-builder__' + modifier + '_form-array', modifier));
};

var InputArray = exports.InputArray = function InputArray(props) {
  var children = props.children,
      name = props.name,
      subject = props.subject,
      formName = props.formName,
      modifier = props.modifier,
      disabled = props.disabled,
      className = props.className,
      formArrayPush = props.formArrayPush,
      isReorderable = props.isReorderable,
      addClassName = props.addClassName;

  console.log('Log deleteIcon:', deleteIcon);
  return _react2.default.createElement(
    'div',
    null,
    _react2.default.createElement(_reduxForm.FieldArray, {
      name: name,
      className: classes(className, modifier),
      subject: subject,
      component: function component(_ref) {
        var fields = _ref.fields;

        var childrenArray = [];
        var fieldsCount = fields.length;

        var _loop = function _loop(i) {
          childrenArray.push(_react2.default.createElement(
            'div',
            {
              className: 'l-flex l-flex_align-end',
              key: 'dynamic-form-' + formName + '-array-column-' + fields.name + '-' + i
            },
            (0, _hUtils.passPropsToChildren)(children, {
              formName: formName,
              namePrefix: fields.name + '[' + i + '].',
              disabled: disabled
            }),
            _react2.default.createElement(
              _mUiToolbox.ButtonGroup,
              null,
              isReorderable && _react2.default.createElement(_mUiToolbox.Button, {
                onClick: function onClick(e) {
                  e.preventDefault();
                  fields.swap(i, i - 1);
                },
                disabled: i == 0,
                theme: 'secondary',
                icon: arrowUpIcon
              }),
              isReorderable && _react2.default.createElement(_mUiToolbox.Button, {
                onClick: function onClick(e) {
                  e.preventDefault();
                  fields.swap(i, i + 1);
                },
                disabled: i + 1 == fields.length,
                theme: 'secondary',
                icon: arrowDownIcon
              }),
              _react2.default.createElement(_mUiToolbox.Button, {
                onClick: function onClick(e) {
                  e.preventDefault();
                  fields.remove(i);
                },
                theme: 'danger',
                icon: deleteIcon
              })
            )
          ));
        };

        for (var i = 0; i < fieldsCount; i++) {
          _loop(i);
        }

        return childrenArray;
      }
    }),
    _react2.default.createElement(
      _mUiToolbox.Button,
      {
        className: (0, _classnames3.default)('u-no-margin', addClassName),
        type: 'button',
        theme: 'secondary',
        key: 'add-array-item-' + name,
        modifier: 'wide',
        onClick: function onClick() {
          formArrayPush(formName, name);
        }
      },
      '+'
    )
  );
};

InputArray.propTypes = {
  formArrayPush: _propTypes2.default.func.isRequired,
  children: _propTypes2.default.node,
  className: _propTypes2.default.string,
  addClassName: _propTypes2.default.string,
  name: _propTypes2.default.string.isRequired,
  subject: _propTypes2.default.object,
  modifier: _propTypes2.default.string
};

exports.default = (0, _reactRedux.connect)(null, {
  formArrayPush: _reduxForm.arrayPush,
  formArrayRemove: _reduxForm.arrayRemove,
  formArrayInsert: _reduxForm.arrayInsert
})(InputArray);