'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.columnClasses = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reduxForm = require('redux-form');

var _reactIntl = require('react-intl');

var _classnames4 = require('classnames');

var _classnames5 = _interopRequireDefault(_classnames4);

var _lodash = require('lodash');

var _autobind = require('utils/autobind');

var _autobind2 = _interopRequireDefault(_autobind);

var _Input = require('components/Inputs/Input');

var _Input2 = _interopRequireDefault(_Input);

var _FormArray = require('components/FormBuilder/FormArray');

var _FormArray2 = _interopRequireDefault(_FormArray);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var classes = function classes(isActive, modifier) {
  return (0, _classnames5.default)('c-form-builder__content', _defineProperty({
    'js-is-hidden': !isActive
  }, 'c-form-builder_' + modifier + '_content', modifier));
};

var rowClasses = function rowClasses(className, modifier) {
  return (0, _classnames5.default)('l-flex', className, _defineProperty({}, 'l-flex_' + modifier, modifier));
};

// Classes for columns of form phase, can be wrapper of inputs or text
var columnClasses = exports.columnClasses = function columnClasses() {
  var width = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
  var className = arguments[1];
  var modifier = arguments[2];
  return (0, _classnames5.default)('l-flex__item', className, 'l-flex__item-' + width, _defineProperty({}, 'l-flex_' + modifier + '_item', modifier));
};

var FormPhase = function (_Component) {
  _inherits(FormPhase, _Component);

  function FormPhase(props) {
    _classCallCheck(this, FormPhase);

    var _this = _possibleConstructorReturn(this, (FormPhase.__proto__ || Object.getPrototypeOf(FormPhase)).call(this, props));

    (0, _autobind2.default)(_this);
    return _this;
  }

  _createClass(FormPhase, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          formRows = _props.formRows,
          isActive = _props.isActive,
          formTitle = _props.formTitle,
          modifier = _props.modifier,
          className = _props.className,
          intl = _props.intl,
          form = _props.form,
          _props$values = _props.values,
          values = _props$values === undefined ? {} : _props$values;


      return _react2.default.createElement(
        'div',
        { className: classes(isActive, modifier, className) },
        formTitle && _react2.default.createElement(
          'div',
          { className: 'u-padding-sides' },
          _react2.default.createElement(
            'h4',
            { className: 't-underlined-header' },
            formTitle.id ? intl.formatMessage(formTitle) : formTitle
          )
        ),
        formRows.map(function (formRow, key) {
          if (!formRow.isVisible || formRow.isVisible(values)) return _react2.default.createElement(
            'div',
            {
              className: rowClasses(formRow.className, formRow.modifier),
              key: 'form-builder-' + key
            },
            formRow.fieldsetTitle && _react2.default.createElement(
              'h5',
              { className: 't-input-legend' },
              formRow.fieldsetTitle.id ? intl.formatMessage(formRow.fieldsetTitle) : formRow.fieldsetTitle
            ),
            formRow.inputs && (0, _lodash.map)(formRow.inputs, function (field, fieldKey) {
              if (!field.input && (!field.isVisible || field.isVisible(values))) {
                var title = field.title,
                    width = field.width,
                    _className = field.className,
                    component = field.component,
                    fieldModifier = field.modifier;

                return _react2.default.createElement(
                  'div',
                  {
                    key: 'form-builder-' + key + '-content-' + fieldKey,
                    className: columnClasses(width, _className, fieldModifier)
                  },
                  component ? component() : title && title.id ? intl.formatMessage(title) : title
                );
              } else if (field.hasOwnProperty('input') && field.hasOwnProperty('arrayName') && field.hasOwnProperty('subject')) {
                var input = field.input,
                    _width = field.width,
                    arrayName = field.arrayName,
                    subject = field.subject,
                    _field$reorderable = field.reorderable,
                    reorderable = _field$reorderable === undefined ? false : _field$reorderable,
                    validate = field.validate;

                return _react2.default.createElement(_reduxForm.FieldArray, {
                  component: _FormArray2.default,
                  data: input,
                  width: _width,
                  reorderable: reorderable,
                  name: arrayName,
                  form: form,
                  subject: subject,
                  index: key,
                  validate: validate,
                  key: 'form-builder-' + key + '-field-array-' + fieldKey
                });
              }

              if (!field.isVisible || field.isVisible(values)) {
                var _field$input = field.input,
                    _component = _field$input.component,
                    children = _field$input.children,
                    name = _field$input.name,
                    _input = _objectWithoutProperties(_field$input, ['component', 'children', 'name']);

                if (field.input.type === 'hidden') {
                  return _react2.default.createElement(
                    _reduxForm.Field,
                    _extends({
                      key: 'phase-form-' + name + '-' + key + '-' + fieldKey
                    }, _input, {
                      name: name,
                      component: _component || _Input2.default
                    }),
                    children
                  );
                }

                return _react2.default.createElement(
                  'div',
                  {
                    className: columnClasses(field.width, field.className, field.modifier),
                    key: 'phase-form-' + name + '-' + key + '-' + fieldKey
                  },
                  field.fieldsetTitle && _react2.default.createElement(
                    'h5',
                    null,
                    field.fieldsetTitle
                  ),
                  _react2.default.createElement(
                    _reduxForm.Field,
                    _extends({
                      key: 'phase-form-' + name + '-' + key + '-' + fieldKey
                    }, _input, {
                      name: name,
                      component: _component || _Input2.default
                    }),
                    children
                  )
                );
              }
            })
          );
        })
      );
    }
  }]);

  return FormPhase;
}(_react.Component);

FormPhase.propTypes = {
  formRows: _propTypes2.default.array.isRequired,
  isActive: _propTypes2.default.bool,
  intl: _propTypes2.default.object.isRequired,
  formTitle: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object]),
  className: _propTypes2.default.string,
  modifier: _propTypes2.default.string,
  values: _propTypes2.default.object
};

exports.default = (0, _reactIntl.injectIntl)(FormPhase);